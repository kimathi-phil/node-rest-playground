import config from 'config';
import connect from './utils/util.connect';
import logger from './utils/util.logger';
import createServer from './utils/util.server';

const main = async () => {
  const port = config.get<number>('port');
  const app = createServer();

  app.listen(port, async () => {
    logger.info(`App is running on: http://localhost:${port}`);

    // DB
    await connect();
  });
};

main().catch((error: Error) => logger.error('Error: ', error.message));
