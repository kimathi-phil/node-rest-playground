import { Router } from 'express';
import requireUser from '../middleware/middleware.require_user';
import {
  createProductSchema,
  deleteProductSchema,
  getProductSchema,
  updateProductSchema,
} from '../schema/schema.product';
import validateResource from '../middleware/middleware.validate_resource';
import {
  createProductHandler,
  deleteProductHandler,
  getProductHandler,
  updateProductHandler,
} from '../controller/controller.product';

const router = Router();

router.post(
  '/',
  [requireUser, validateResource(createProductSchema)],
  createProductHandler,
);

router.get(
  '/:productId',
  validateResource(getProductSchema),
  getProductHandler,
);

router.put(
  '/:productId',
  [requireUser, validateResource(updateProductSchema)],
  updateProductHandler,
);

router.delete(
  '/:productId',
  [requireUser, validateResource(deleteProductSchema)],
  deleteProductHandler,
);

export default router;
