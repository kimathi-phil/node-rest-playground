import { Router } from 'express';
import {
  createSessionHandler,
  getUserSessionsHandler,
  deleteSessionHandler,
} from '../controller/controller.session';
import validateResource from '../middleware/middleware.validate_resource';
import { createSessionSchema } from '../schema/schema.session';
import requireUser from '../middleware/middleware.require_user';

const router = Router();

router.post('/', validateResource(createSessionSchema), createSessionHandler);
router.get('/', requireUser, getUserSessionsHandler);
router.delete('/', requireUser, deleteSessionHandler);

export default router;
