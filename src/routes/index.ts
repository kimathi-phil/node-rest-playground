import { Express, Request, Response } from 'express';
import { apiResponse } from '../utils/util.api_response';
import userRoutes from './route.user';
import sessionRoutes from './route.session';
import productRoutes from './route.product';

function routes(app: Express) {
  const version = '/api/v1';

  app.all(['/', `${version}`], (_req: Request, res: Response) =>
    res.status(200).json(apiResponse(200, 'Server Ping')),
  );

  app.get(
    ['/healthcheck', `${version}/healthcheck`],
    (req: Request, res: Response) => res.sendStatus(200),
  );

  app.use(`${version}/users`, userRoutes);
  app.use(`${version}/sessions`, sessionRoutes);
  app.use(`${version}/products`, productRoutes);
}

export default routes;
