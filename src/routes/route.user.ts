import { Router } from 'express';
import { createUserHandler } from '../controller/controller.user';
import validateResource from '../middleware/middleware.validate_resource';
import { createUserSchema } from '../schema/schema.user';

const router = Router();

router.post('/', validateResource(createUserSchema), createUserHandler);

export default router;
