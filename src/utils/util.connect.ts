import mongoose from 'mongoose';
import config from 'config';
import logger from './util.logger';

export default async function connect() {
  const dbUri = config.get<string>('dbUri');

  try {
    await mongoose.connect(dbUri);
    logger.info('Database connected');
  } catch (error: any) {
    logger.error('Could not connect to the Database');
    logger.error(`Error: ${error.message}`);
    process.exit(1);
  }
}
