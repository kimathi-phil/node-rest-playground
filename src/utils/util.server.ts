import express from 'express';
import routes from '../routes';
import deserializeUser from '../middleware/middleware.deserialize_user';

const createServer = () => {
  const app = express();

  // MIDDLEWARE
  app.use(express.json());
  app.use(deserializeUser);

  // ROUTES
  routes(app);

  return app;
};

export default createServer;
