import { Request, Response } from 'express';
import { CreateUserInput } from '../schema/schema.user';
import { createUser } from '../service/service.user';
import logger from '../utils/util.logger';

export async function createUserHandler(
  req: Request<{}, {}, CreateUserInput['body']>,
  res: Response,
) {
  try {
    const user = await createUser(req.body);
    return res.send(user);
  } catch (error: any) {
    logger.error(`Error: ${error}`);
    return res.status(409).send(error.message);
  }
}
