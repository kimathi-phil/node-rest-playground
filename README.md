#  Node REST Playground

[![CI Workflow](https://github.com/kimathi-phil/node-rest-playground/actions/workflows/ci.yaml/badge.svg)](https://github.com/kimathi-phil/node-rest-playground/actions/workflows/ci.yaml)
[![codecov](https://codecov.io/gh/kimathi-phil/node-rest-playground/branch/main/graph/badge.svg?token=qefBKfq3FN)](https://codecov.io/gh/kimathi-phil/node-rest-playground)

A playground repo to learn and practice all matters REST in Node
